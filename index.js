const express = require('express');
var bodyParser = require('body-parser')
var cors = require('cors')

const app = express();
const port = process.env.PORT || 3000

app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
    );
    next();
});
app.use('/app/public/images', express.static(__dirname + '/app/public/images/'));
app.use(cors())
app.use('/', require('./app/routes/index'));

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))