'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert('Users',
    [
      {
        username: 'Elvis',
        fullname: 'Elvis Huges do Nasciemento',
        email: 'elvis@medium.com',
        password: '123',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: 'Jaque',
        fullname: 'Jaque Huges do Nasciemento',
        email: 'jaque@medium.com',
        password: '123',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {}),

  down: (queryInterface) => queryInterface.bulkDelete('Users', null, {}),
};
