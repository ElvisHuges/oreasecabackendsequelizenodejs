'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert('Posts',
    [
      {
        title: 'Elvis',
        description: 'Elvis Huges do Nasciemento',
        andress: 'elvis@medium.com',
        status: 0,
        id_user: 1,
        logicDeleted: 0,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Title test',
        description: 'Description test',
        andress: 'Andress test',
        status: 0,
        id_user: 1,
        logicDeleted: 0,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {}),

  down: (queryInterface) => queryInterface.bulkDelete('Users', null, {}),
};
