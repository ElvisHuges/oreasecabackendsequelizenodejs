const authJwt = require("./authJwt");
const uploadFiles = require("./uploadFiles");

module.exports = {
    authJwt,
    uploadFiles
};