
module.exports = (sequelize, DataTypes) => {
    const Post = sequelize.define('Post', {
        title: DataTypes.STRING,
        description: DataTypes.STRING,
        andress: DataTypes.STRING,
        dirImage: DataTypes.STRING,
        id_user: DataTypes.INTEGER,
        status: DataTypes.INTEGER,
        logicDeleted: DataTypes.STRING
    });
    Post.associate = function (models) {
        Post.belongsTo(models.User, { foreignKey: 'id_user', as: 'users' })
    };
    return Post;
}