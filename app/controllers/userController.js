const { Post } = require('../models');
const { User } = require('../models');

module.exports = {

    async findAll(req, res) {
        const users = await User.findAll({ include: 'posts' });
        return res.json(users);
    },
}