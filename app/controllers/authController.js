const { User } = require('../models');
var bcrypt = require("bcryptjs");
var jwt = require("jsonwebtoken");
const config = require("./../../config/auth.config");


exports.getUserById = async (idUser) => {
    return User.findByPk(idUser);
}

exports.signup = (req, res) => {
    // Save User to Database
    User.create({
        username: req.body.username,
        fullname: req.body.fullname,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 8)
    })
        .then(usr => {
            var token = jwt.sign({ user: usr }, config.secret, {
                expiresIn: 86400 // 24 hours
            });
            if (usr) {
                var user = {
                    username: usr.username,
                    fullname: usr.fullname,
                    email: usr.email,
                }
                return res.status(201).json({
                    user,
                    token
                });
            }
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

exports.signin = (req, res) => {
    User.findOne({
        where: {
            username: req.body.username
        }
    })
        .then(usr => {
            if (!usr) {
                return res.status(404).send({ message: "User Not found." });
            }
            var passwordIsValid = bcrypt.compareSync(
                req.body.password,
                usr.password
            );

            if (!passwordIsValid) {
                return res.status(401).send({
                    accessToken: null,
                    message: "Invalid Password!"
                });
            }

            var token = jwt.sign({ user: usr }, config.secret, {
                expiresIn: 86400 // 24 hours
            });
            var user = {
                username: usr.username,
                fullname: usr.fullname,
                email: usr.email,
            }
            return res.status(201).json({
                user,
                token
            });
        });
};



