const { Post } = require('../models');
const db = require("../models");
const Users = db.users;

module.exports = {

    async findAll(req, res) {
        try {
            const posts = await Post.findAll({
                include: [
                    {
                        model: Users,
                        as: 'users',
                        attributes: ["fullname"]
                    }]
            });
            return res.status(201).json({
                posts,
            });
        } catch (error) {
            console.log('error', error)
            return res.status(500).json({ error: error.message })
        }
    },

    async create(req, res) {
        console.log("req userId", req.file);
        const userDecodedToken = req.user
        const postAux = {
            title: req.query.title,
            description: req.query.description,
            andress: req.query.andress,
            status: req.query.status,
            dirImage: req.file.destination + "/" + req.file.filename,
            id_user: userDecodedToken.id
        }
        try {
            const post = await Post.create(postAux);
            return res.status(201).json({
                post,
            });
        } catch (error) {
            return res.status(500).json({ error: error.message })
        }
    },

    async findOne(req, res) {
        try {
            const { postId } = req.params;
            const post = await Post.findOne({
                where: { id: postId }, include: [
                    {
                        model: Users,
                        as: 'users',
                        attributes: ["fullname"]
                    }]
            });
            if (post) {
                return res.status(200).json({ post });
            }
            return res.status(404).send('Post with the specified ID does not exists');
        } catch (error) {
            return res.status(500).send(error.message);
        }
    },
}