const postController = require("../controllers/postController");
const { uploadFiles, authJwt } = require("./../middlewares");
const express = require('express');
const router = express.Router();

router.get("/", postController.findAll);
router.get('/:postId', postController.findOne);
router.post('/', authJwt.verifyToken, uploadFiles, postController.create);

module.exports = router;
