const express = require('express');
const router = express.Router();

const postRoutes = require('./postRoutes');
const userRoutes = require('./userRoutes');
const authRoute = require('./authRoutes');

router.get('/', (req, res) => {
    res.json({ message: 'welcome to api OreaSeca!' })
})
    .use('/auth', authRoute)
    .use('/posts', postRoutes)
    .use('/users', userRoutes);

module.exports = router;